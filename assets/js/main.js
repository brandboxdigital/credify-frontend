$(function(){
    var accordion1 = new Accordion();
    var modals = new Modal();
    var oc = new OffCanvas();
    
    var mm = new MobileMenu();

    svg4everybody();

    $('.input--select').select2();

    try_animation_wrap(credify_symbol__init);
    try_animation_wrap(credify_icon1__init);
    try_animation_wrap(credify_icon2__init);
    try_animation_wrap(credify_icon3__init);        
    try_animation_wrap(credify_main_image__init);        
})

function try_animation_wrap(fun) {
    try {
        fun();        
    } catch(e) {
        // console.log(e);
    }
}

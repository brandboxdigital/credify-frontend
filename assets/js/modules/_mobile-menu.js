MobileMenu = function() {
    var me = this;

    me.visibility_class = 'is-open';

    me.init = function (arguments) {
        $(document).on('click', '[data-close-mobilemenu]', me.closeMenu)
        $('[data-open-mobilemenu]').on('click', me.openMenu);
        
        $('.mobile-menu__sidemenu').on('click', '.has-submenu > a', me.toggleDropDown);
    }

    me.openMenu = function(e) {
        // Maybe PreventDefault would be goo idea
        e.preventDefault();
        $('.mobile-menu__sidemenu').addClass(me.visibility_class)
    }
    me.closeMenu = function() {
        $('.mobile-menu__sidemenu').removeClass(me.visibility_class);
    }

    me.toggleDropDown = function(e) {
        e.preventDefault();
        $(this).closest('.has-submenu').toggleClass(me.visibility_class)
    }

    me.init();
};

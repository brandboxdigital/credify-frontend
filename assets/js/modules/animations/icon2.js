(function (cjs, an) {
var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];
// symbols:
(lib.Symbol9 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#FFFFFF").ss(3,1,1).p("AAEAGIgHgL");
    this.shape.setTransform(-3.1,-1);
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f().s("#FFFFFF").ss(3,1,1).p("AgEgHIAJAP");
    this.shape_1.setTransform(-3,-0.8);
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f().s("#FFFFFF").ss(3,1,1).p("AgJgOIATAd");
    this.shape_2.setTransform(-2.5,-0.1);
    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f().s("#FFFFFF").ss(3,1,1).p("AARAaIghgz");
    this.shape_3.setTransform(-1.8,1);
    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).wait(6));
    // Layer_1
    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f().s("#FFFFFF").ss(3,1,1).p("AAFgJIgJAT");
    this.shape_4.setTransform(0.4,2.5);
    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f().s("#FFFFFF").ss(3,1,1).p("AgJATIATgl");
    this.shape_5.setTransform(0.9,1.6);
    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f().s("#FFFFFF").ss(3,1,1).p("AgMAaIAagz");
    this.shape_6.setTransform(1.3,0.9);
    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f().s("#FFFFFF").ss(3,1,1).p("AgPAfIAfg9");
    this.shape_7.setTransform(1.5,0.4);
    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f().s("#FFFFFF").ss(3,1,1).p("AgRAiIAihD");
    this.shape_8.setTransform(1.7,0.1);
    this.shape_9 = new cjs.Shape();
    this.shape_9.graphics.f().s("#FFFFFF").ss(3,1,1).p("AASgiIgjBF");
    this.shape_9.setTransform(1.7,0);
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).wait(1));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5,-3.1,3.8,4.1);
(lib.Symbol8 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#2AD2CA").s().p("AkIBfQgnAAgdgcQgbgbAAgnIAAgBQAAgnAbgcQAcgbAlAAIIWAAQAmAAAaAbQAdAcAAAnIAAABQAAAngdAbQgbAcgnAAg");
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36,-9.5,72,19);
(lib.Symbol7 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#FFFFFF").s().p("AkIBfQgnAAgdgcQgbgbAAgnIAAgBQAAgnAbgcQAcgbAlAAIIWAAQAmAAAaAbQAdAcAAAnIAAABQAAAngdAbQgbAcgnAAg");
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36,-9.5,72,19);
(lib.Symbol6 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_2
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#CEDAE1").ss(3,1,1).p("AgFgFIALAL");
    this.shape.setTransform(-2.9,-2.9);
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f().s("#CEDAE1").ss(3,1,1).p("AgVgVIArAr");
    this.shape_1.setTransform(-1.3,-1.3);
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f().s("#CEDAE1").ss(3,1,1).p("AgfgfIA/A/");
    this.shape_2.setTransform(-0.3,-0.3);
    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f().s("#CEDAE1").ss(3,1,1).p("AgigiIBFBF");
    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).wait(5));
    // Layer_1
    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f().s("#CEDAE1").ss(3,1,1).p("AAGgFIgLAL");
    this.shape_4.setTransform(2.9,-2.8);
    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f().s("#CEDAE1").ss(3,1,1).p("AgSATIAlgl");
    this.shape_5.setTransform(1.6,-1.6);
    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f().s("#CEDAE1").ss(3,1,1).p("AgbAcIA3g3");
    this.shape_6.setTransform(0.7,-0.7);
    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f().s("#CEDAE1").ss(3,1,1).p("AggAhIBBhB");
    this.shape_7.setTransform(0.2,-0.2);
    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f().s("#CEDAE1").ss(3,1,1).p("AAjgiIhFBF");
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).wait(1));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5,-5,4.2,4.2);
(lib.Symbol4 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#F5F9FB").s().p("AoEIEQjVjWAAkuQAAktDVjXQDXjVEtAAQEvAADVDVQDWDXAAEtQAAEujWDWQjVDWkvAAQktAAjXjWg");
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-73,-73,146,146);
// stage content:
(lib.credify_mid_2 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_3
    this.instance = new lib.Symbol6("synched",0,false);
    this.instance.parent = this;
    this.instance.setTransform(92.6,75.1);
    this.instance._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(197).to({_off:false},0).wait(48).to({startPosition:7},0).to({regY:-0.4,scaleY:0.14},4,cjs.Ease.get(-1)).to({_off:true},1).wait(6));
    // Layer_4
    this.instance_1 = new lib.Symbol6("synched",0,false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(92.6,100.1);
    this.instance_1._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(207).to({_off:false},0).wait(40).to({startPosition:7},0).to({scaleY:0.14},4,cjs.Ease.get(-1)).to({_off:true},1).wait(4));
    // Layer_5
    this.instance_2 = new lib.Symbol9("synched",0,false);
    this.instance_2.parent = this;
    this.instance_2.setTransform(92.6,50.1);
    this.instance_2._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(219).to({_off:false},0).wait(24).to({startPosition:8},0).to({regY:0.4,scaleY:0.14},4,cjs.Ease.get(-1)).to({_off:true},1).wait(8));
    // Layer_6
    this.instance_3 = new lib.Symbol8("synched",0);
    this.instance_3.parent = this;
    this.instance_3.setTransform(73,50.1);
    this.instance_3.alpha = 0;
    this.instance_3._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(215).to({_off:false},0).to({alpha:1},4,cjs.Ease.get(-1)).wait(24).to({startPosition:0},0).to({regY:0.3,scaleY:0.16},4,cjs.Ease.get(-1)).to({_off:true},1).wait(8));
    // Layer_7
    this.instance_4 = new lib.Symbol6("synched",0,false);
    this.instance_4.parent = this;
    this.instance_4.setTransform(92.6,50.1);
    this.instance_4._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(133).to({_off:false},0).wait(46).to({startPosition:7},0).to({regY:-0.4,scaleY:0.14},4,cjs.Ease.get(-1)).to({_off:true},1).wait(72));
    // Layer_8
    this.instance_5 = new lib.Symbol6("synched",0,false);
    this.instance_5.parent = this;
    this.instance_5.setTransform(92.6,100.1);
    this.instance_5._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(145).to({_off:false},0).wait(38).to({startPosition:7},0).to({scaleY:0.14},4,cjs.Ease.get(-1)).to({_off:true},1).wait(68));
    // Layer_9
    this.instance_6 = new lib.Symbol9("synched",0,false);
    this.instance_6.parent = this;
    this.instance_6.setTransform(92.6,75.1);
    this.instance_6._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(159).to({_off:false},0).wait(22).to({startPosition:8},0).to({regY:0.4,scaleY:0.14},4,cjs.Ease.get(-1)).to({_off:true},1).wait(70));
    // Layer_10
    this.instance_7 = new lib.Symbol8("synched",0);
    this.instance_7.parent = this;
    this.instance_7.setTransform(73,75.1);
    this.instance_7.alpha = 0;
    this.instance_7._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(155).to({_off:false},0).to({alpha:1},4,cjs.Ease.get(-1)).wait(22).to({startPosition:0},0).to({regY:0.3,scaleY:0.16},4,cjs.Ease.get(-1)).to({_off:true},1).wait(70));
    // Layer_11
    this.instance_8 = new lib.Symbol6("synched",0,false);
    this.instance_8.parent = this;
    this.instance_8.setTransform(92.6,75.1);
    this.instance_8._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(68).to({_off:false},0).wait(49).to({startPosition:7},0).to({regY:-0.4,scaleY:0.14},4,cjs.Ease.get(-1)).to({_off:true},1).wait(134));
    // Layer_12
    this.instance_9 = new lib.Symbol6("synched",0,false);
    this.instance_9.parent = this;
    this.instance_9.setTransform(92.6,100.1);
    this.instance_9._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(80).to({_off:false},0).wait(39).to({startPosition:7},0).to({scaleY:0.14},4,cjs.Ease.get(-1)).to({_off:true},1).wait(132));
    // Layer_13
    this.instance_10 = new lib.Symbol9("synched",0,false);
    this.instance_10.parent = this;
    this.instance_10.setTransform(92.6,50.1);
    this.instance_10._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(94).to({_off:false},0).wait(21).to({startPosition:8},0).to({regY:0.4,scaleY:0.14},4,cjs.Ease.get(-1)).to({_off:true},1).wait(136));
    // Layer_14
    this.instance_11 = new lib.Symbol8("synched",0);
    this.instance_11.parent = this;
    this.instance_11.setTransform(73,50.1);
    this.instance_11.alpha = 0;
    this.instance_11._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(90).to({_off:false},0).to({alpha:1},4,cjs.Ease.get(-1)).wait(21).to({startPosition:0},0).to({regY:0.3,scaleY:0.16},4,cjs.Ease.get(-1)).to({_off:true},1).wait(136));
    // Layer_15
    this.instance_12 = new lib.Symbol6("synched",0,false);
    this.instance_12.parent = this;
    this.instance_12.setTransform(92.6,50.1);
    this.instance_12._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(5).to({_off:false},0).wait(46).to({startPosition:7},0).to({regY:-0.4,scaleY:0.14},4,cjs.Ease.get(-1)).to({_off:true},1).wait(200));
    // Layer_16
    this.instance_13 = new lib.Symbol6("synched",0,false);
    this.instance_13.parent = this;
    this.instance_13.setTransform(92.6,75.1);
    this.instance_13._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(17).to({_off:false},0).wait(36).to({startPosition:7},0).to({scaleY:0.14},4,cjs.Ease.get(-1)).to({_off:true},1).wait(198));
    // Layer_17
    this.instance_14 = new lib.Symbol9("synched",0,false);
    this.instance_14.parent = this;
    this.instance_14.setTransform(92.6,100.1);
    this.instance_14._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(31).to({_off:false},0).wait(24).to({startPosition:8},0).to({regY:0.4,scaleY:0.14},4,cjs.Ease.get(-1)).to({_off:true},1).wait(196));
    // Layer_18
    this.instance_15 = new lib.Symbol8("synched",0);
    this.instance_15.parent = this;
    this.instance_15.setTransform(73,100.1);
    this.instance_15.alpha = 0;
    this.instance_15._off = true;
    this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(27).to({_off:false},0).to({alpha:1},4,cjs.Ease.get(-1)).wait(24).to({startPosition:0},0).to({regY:0.3,scaleY:0.16},4,cjs.Ease.get(-1)).to({_off:true},1).wait(196));
    // Symbol 7
    this.instance_16 = new lib.Symbol7("synched",0);
    this.instance_16.parent = this;
    this.instance_16.setTransform(73,50.1);
    this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(51).to({startPosition:0},0).to({regY:-0.3,scaleY:0.16},4,cjs.Ease.get(-1)).to({regY:0,scaleY:1},4,cjs.Ease.get(1)).wait(56).to({startPosition:0},0).to({regY:-0.3,scaleY:0.16},4,cjs.Ease.get(-1)).to({regY:0,scaleY:1},4,cjs.Ease.get(1)).wait(56).to({startPosition:0},0).to({regY:-0.3,scaleY:0.16},4,cjs.Ease.get(-1)).to({regY:0,scaleY:1},4,cjs.Ease.get(1)).wait(56).to({startPosition:0},0).to({regY:-0.3,scaleY:0.16},4,cjs.Ease.get(-1)).to({regY:0,scaleY:1},4,cjs.Ease.get(1)).wait(5));
    // Symbol 7
    this.instance_17 = new lib.Symbol7("synched",0);
    this.instance_17.parent = this;
    this.instance_17.setTransform(73,75.1);
    this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(53).to({startPosition:0},0).to({scaleY:0.16},4,cjs.Ease.get(-1)).to({scaleY:1},4,cjs.Ease.get(1)).wait(56).to({startPosition:0},0).to({scaleY:0.16},4,cjs.Ease.get(-1)).to({scaleY:1},4,cjs.Ease.get(1)).wait(56).to({startPosition:0},0).to({scaleY:0.16},4,cjs.Ease.get(-1)).to({scaleY:1},4,cjs.Ease.get(1)).wait(56).to({startPosition:0},0).to({scaleY:0.16},4,cjs.Ease.get(-1)).to({scaleY:1},4,cjs.Ease.get(1)).wait(3));
    // Symbol 7
    this.instance_18 = new lib.Symbol7("synched",0);
    this.instance_18.parent = this;
    this.instance_18.setTransform(73,100.1);
    this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(55).to({startPosition:0},0).to({regY:0.3,scaleY:0.16},4,cjs.Ease.get(-1)).to({regY:0,scaleY:1},4,cjs.Ease.get(1)).wait(56).to({startPosition:0},0).to({regY:0.3,scaleY:0.16},4,cjs.Ease.get(-1)).to({regY:0,scaleY:1},4,cjs.Ease.get(1)).wait(56).to({startPosition:0},0).to({regY:0.3,scaleY:0.16},4,cjs.Ease.get(-1)).to({regY:0,scaleY:1},4,cjs.Ease.get(1)).wait(56).to({startPosition:0},0).to({regY:0.3,scaleY:0.16},4,cjs.Ease.get(-1)).to({regY:0,scaleY:1},4,cjs.Ease.get(1)).wait(1));
    // Layer_22
    this.instance_19 = new lib.Symbol4("synched",0);
    this.instance_19.parent = this;
    this.instance_19.setTransform(73,73);
    this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(256));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(73,73,146,146);
// library properties:
lib.properties = {
    id: '0BA959A6F261BF4788F650C9EFA302F7',
    width: 146,
    height: 146,
    fps: 25,
    color: "#FFFFFF",
    opacity: 0.00,
    manifest: [],
    preloads: []
};
// bootstrap callback support:
(lib.Stage = function(canvas) {
    createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();
p.setAutoPlay = function(autoPlay) {
    this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }
p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }
an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
    an.bootstrapListeners=[];
}
an.bootstrapCallback=function(fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if(an.bootcompsLoaded.length > 0) {
        for(var i=0; i<an.bootcompsLoaded.length; ++i) {
            fnCallback(an.bootcompsLoaded[i]);
        }
    }
};
an.compositions = an.compositions || {};
an.compositions['0BA959A6F261BF4788F650C9EFA302F7'] = {
    getStage: function() { return exportRoot.getStage(); },
    getLibrary: function() { return lib; },
    getSpriteSheet: function() { return ss; },
    getImages: function() { return img; }
};
an.compositionLoaded = function(id) {
    an.bootcompsLoaded.push(id);
    for(var j=0; j<an.bootstrapListeners.length; j++) {
        an.bootstrapListeners[j](id);
    }
}
an.getComposition = function(id) {
    return an.compositions[id];
}
})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

//---------------------------------------------------------------//
//

var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
function credify_icon2__init() {
    canvas = document.getElementById("credify_icon2__canvas");
    anim_container = document.getElementById("credify_icon2__animation_container");
    dom_overlay_container = document.getElementById("credify_icon2__dom_overlay_container");
    var comp=AdobeAn.getComposition("0BA959A6F261BF4788F650C9EFA302F7");
    var lib=comp.getLibrary();
    credify_icon2__handleComplete({},comp);
}
function credify_icon2__handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    exportRoot = new lib.credify_mid_2();
    stage = new lib.Stage(canvas);  
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
        stage.addChild(exportRoot);
        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    }       
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {      
        var lastW, lastH, lastS=1;      
        window.addEventListener('resize', resizeCanvas);        
        resizeCanvas();     
        function resizeCanvas() {           
            var w = lib.properties.width, h = lib.properties.height;            
            var iw = window.innerWidth, ih=window.innerHeight;          
            var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;          
            if(isResp) {                
                if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
                    sRatio = lastS;                
                }               
                else if(!isScale) {                 
                    if(iw<w || ih<h)                        
                        sRatio = Math.min(xRatio, yRatio);              
                }               
                else if(scaleType==1) {                 
                    sRatio = Math.min(xRatio, yRatio);              
                }               
                else if(scaleType==2) {                 
                    sRatio = Math.max(xRatio, yRatio);              
                }           
            }           
            canvas.width = w*pRatio*sRatio;         
            canvas.height = h*pRatio*sRatio;
            canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';               
            canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
            stage.scaleX = pRatio*sRatio;           
            stage.scaleY = pRatio*sRatio;           
            lastW = iw; lastH = ih; lastS = sRatio;            
            stage.tickOnUpdate = false;            
            stage.update();            
            stage.tickOnUpdate = true;      
        }
    }
    makeResponsive(true,'height',true,1); 
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
}

(function (cjs, an) {
var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];
// symbols:
(lib.Symbol5 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#2BD2CA").s().p("Ag1A1QgVgWAAgfQAAgeAVgWQAXgWAeAAQAfAAAVAWQAXAWAAAeQAAAfgXAWQgVAWgfAAQgeAAgXgWg");
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.5,-7.5,15,15);
(lib.Symbol4 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#F5F9FB").s().p("AoEIEQjVjWAAkuQAAktDVjXQDXjVEtAAQEvAADVDVQDWDXAAEtQAAEujWDWQjVDWkvAAQktAAjXjWg");
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-73,-73,146,146);
// stage content:
(lib.credify_mid_1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_3
    this.instance = new lib.Symbol5("synched",0);
    this.instance.parent = this;
    this.instance.setTransform(89.5,45.6);
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({startPosition:0},0).to({x:80.5},8,cjs.Ease.get(-1)).to({x:71.5},9,cjs.Ease.get(1)).wait(23).to({startPosition:0},0).to({x:62.4,y:45.8},8,cjs.Ease.get(-1)).to({x:53.3,y:46},9,cjs.Ease.get(1)).wait(23).to({startPosition:0},0).to({x:71.4,y:45.8},8,cjs.Ease.get(-1)).to({x:89.5,y:45.6},9,cjs.Ease.get(1)).wait(14));
    // Layer_4
    this.instance_1 = new lib.Symbol5("synched",0);
    this.instance_1.parent = this;
    this.instance_1.setTransform(57.7,66);
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(12).to({startPosition:0},0).to({x:75},8,cjs.Ease.get(-1)).to({x:92.3},9,cjs.Ease.get(1)).wait(23).to({startPosition:0},0).to({x:77.7},8,cjs.Ease.get(-1)).to({x:63},9,cjs.Ease.get(1)).wait(23).to({startPosition:0},0).to({x:60.4},8,cjs.Ease.get(-1)).to({x:57.7},9,cjs.Ease.get(1)).wait(6));
    // Layer_5
    this.shape = new cjs.Shape();
    this.shape.graphics.f().s("#FFFFFF").ss(5,0,1).p("AhPBpIkYAAAFoAAIrPAAAFohoIrPAA");
    this.shape.setTransform(73,96.6);
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f().s("#DFE7ED").ss(5,1,1).p("AlOBkIKdAAAlOhjIKdAA");
    this.shape_1.setTransform(73,56);
    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(115));
    // Layer_6
    this.instance_2 = new lib.Symbol4("synched",0);
    this.instance_2.parent = this;
    this.instance_2.setTransform(73,73);
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(115));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(73,73,146,146);
// library properties:
lib.properties = {
    id: '49E5444DD9145843BE9220F34A396BC8',
    width: 146,
    height: 146,
    fps: 25,
    color: "#FFFFFF",
    opacity: 0.00,
    manifest: [],
    preloads: []
};
// bootstrap callback support:
(lib.Stage = function(canvas) {
    createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();
p.setAutoPlay = function(autoPlay) {
    this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }
p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }
an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
    an.bootstrapListeners=[];
}
an.bootstrapCallback=function(fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if(an.bootcompsLoaded.length > 0) {
        for(var i=0; i<an.bootcompsLoaded.length; ++i) {
            fnCallback(an.bootcompsLoaded[i]);
        }
    }
};
an.compositions = an.compositions || {};
an.compositions['49E5444DD9145843BE9220F34A396BC8'] = {
    getStage: function() { return exportRoot.getStage(); },
    getLibrary: function() { return lib; },
    getSpriteSheet: function() { return ss; },
    getImages: function() { return img; }
};
an.compositionLoaded = function(id) {
    an.bootcompsLoaded.push(id);
    for(var j=0; j<an.bootstrapListeners.length; j++) {
        an.bootstrapListeners[j](id);
    }
}
an.getComposition = function(id) {
    return an.compositions[id];
}
})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

//---------------------------------------------------------------//
//

var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
function credify_icon1__init() {
    canvas = document.getElementById("credify_icon1__canvas");
    anim_container = document.getElementById("credify_icon1__animation_container");
    dom_overlay_container = document.getElementById("credify_icon1__dom_overlay_container");
    var comp=AdobeAn.getComposition("49E5444DD9145843BE9220F34A396BC8");
    var lib=comp.getLibrary();
    credify_icon1__handleComplete({},comp);
}
function credify_icon1__handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    exportRoot = new lib.credify_mid_1();
    stage = new lib.Stage(canvas);  
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
        stage.addChild(exportRoot);
        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    }       
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {      
        var lastW, lastH, lastS=1;      
        window.addEventListener('resize', resizeCanvas);        
        resizeCanvas();     
        function resizeCanvas() {           
            var w = lib.properties.width, h = lib.properties.height;            
            var iw = window.innerWidth, ih=window.innerHeight;          
            var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;          
            if(isResp) {                
                if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
                    sRatio = lastS;                
                }               
                else if(!isScale) {                 
                    if(iw<w || ih<h)                        
                        sRatio = Math.min(xRatio, yRatio);              
                }               
                else if(scaleType==1) {                 
                    sRatio = Math.min(xRatio, yRatio);              
                }               
                else if(scaleType==2) {                 
                    sRatio = Math.max(xRatio, yRatio);              
                }           
            }           
            canvas.width = w*pRatio*sRatio;         
            canvas.height = h*pRatio*sRatio;
            canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';               
            canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
            stage.scaleX = pRatio*sRatio;           
            stage.scaleY = pRatio*sRatio;           
            lastW = iw; lastH = ih; lastS = sRatio;            
            stage.tickOnUpdate = false;            
            stage.update();            
            stage.tickOnUpdate = true;      
        }
    }
    makeResponsive(true,'height',true,1); 
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
}

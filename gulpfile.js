var gulp       = require('gulp'),
    // browserify = require('browserify'),
    // tap        = require('gulp-tap'),
    // source     = require('vinyl-source-stream'),
    // buffer     = require('vinyl-buffer'),
    minify     = require("gulp-babel-minify"),
    // babelify   = require('babelify'),
    sourcemaps = require('gulp-sourcemaps'),
    concat     = require('gulp-concat');

var autoprefix  = require("gulp-autoprefixer"),
    browserSync = require('browser-sync').create(),    
    sass        = require("gulp-sass"),
    bourbon     = require("bourbon").includePaths;
    // neat        = require("bourbon-neat").includePaths;

var twig = require('gulp-twig');

var sassPaths = [
  'assets/scss/',
  bourbon
];

/**
 * ===============================================================
 * Compiling sass files
 */
gulp.task("sass-env-dev", function () {
  return gulp.src('assets/scss/main.scss')
    .pipe(sass({
          sourcemaps: true,
          includePaths: sassPaths,
      })
      .on('error', sass.logError)
    )
    .pipe(autoprefix("last 2 versions"))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.stream());
});

gulp.task("sass-build", function () {
  return gulp.src('assets/scss/main.scss')
    .pipe(sass({
          sourcemaps: true,
          includePaths: sassPaths,
          outputStyle: 'compressed',
      })
      .on('error', sass.logError)
    )
    .pipe(autoprefix("last 4 versions"))
    .pipe(gulp.dest('assets/css'))
});

/**
 * ===============================================================
 * Compiling js files and running then trough babel
 */
gulp.task('js-env-dev', function(done) {
  return gulp.src(['assets/js/modules/**/*.js', 'assets/js/main.js'])
    .pipe(concat('main.js'))
    // .pipe(babel({presets: ["env"]}))
    .pipe(gulp.dest('assets/js/min'))
    .pipe(browserSync.stream());


  // return browserify(['assets/js/main.js'], {debug: true})
  //   .transform('babelify', {presets: ["env"]})
  //   .bundle()
  //   .pipe(source('main.js')) // Readable Stream -> Stream Of Vinyl Files
  //   .pipe(buffer()) // Vinyl Files -> Buffered Vinyl Files
  //   .pipe(gulp.dest('assets/js/min'))
  //   .pipe(browserSync.stream());
});

gulp.task('js-build', function() {
  return gulp.src(['assets/js/modules/**/*.js', 'assets/js/main.js'])
    .pipe(concat('main.js'))
    // .pipe(babel({presets: ["env"]}))
    .pipe(sourcemaps.init())
    .pipe(minify({
      mangle: {
        keepClassName: true,
        keepFnName: true
      }
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('assets/js/min'))

  // return browserify(['assets/js/main.js'])
  //     .transform('babelify', {presets: ["env"]})
  //     .bundle()
  //     .pipe(source('main.js')) // Readable Stream -> Stream Of Vinyl Files
  //     .pipe(buffer()) // Vinyl Files -> Buffered Vinyl Files
  //     .pipe(sourcemaps.init())
  //     .pipe(minify({
  //       mangle: {
  //         keepClassName: true,
  //         keepFnName: true
  //       }
  //     }))
  //     .pipe(sourcemaps.write('.'))
  //     .pipe(gulp.dest('assets/js/min'))
});

gulp.task('js-vendor', function(done) {
  return gulp.src(['assets/js/vendor/*.js'])
    .pipe(concat('vendor.js'))
    // .pipe(babel({presets: ["env"]}))
    .pipe(gulp.dest('assets/js/min'))
    .pipe(browserSync.stream());
});


/**
 * ======================
 * HTML + TWIG
 */
gulp.task('html-compile', function () {
    
    return gulp.src('twig/pages/*.twig')
      .pipe(twig({
          base: './twig'
      }))
      .pipe(gulp.dest('./'))
      .pipe(browserSync.stream());
});

/**
 * ===============================================================
 * Calling Browser Sync And watching for changes
 */
gulp.task('watcher', function() {
  browserSync.init({
      server: {
          baseDir: "./"
      }
      // proxy: "demo.vagrant.test",
  });

  gulp.watch(['assets/scss/**/*.scss'], gulp.series(['sass-env-dev']));

  gulp.watch(['assets/js/vendor/**/*.js'], gulp.series(['js-vendor']));
  gulp.watch(['assets/js/main.js','assets/js/modules/**/*.js'], gulp.series(['js-env-dev']));
  
  gulp.watch(['twig/**/*.twig'], gulp.series(['html-compile']));
  // gulp.watch(['**/*.html', '**/*.htm']).on('change', browserSync.reload);
});


gulp.task( "default", gulp.series(['html-compile', 'sass-env-dev', 'js-vendor', 'js-env-dev', 'watcher']) );
gulp.task( "build", gulp.series(['html-compile', 'sass-build', 'js-vendor', 'js-build']) );



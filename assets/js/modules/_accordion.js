Accordion = function() {
    var me = this;

    me.init = function() {
        $('[data-accordion]').on('click', '.accordion__title', me.toggle);
    }

    me.toggle = function() {
        var accordion = $(this).closest('[data-accordion]');
        var item = $(this).closest('.accordion__item');
        var isOpen = item.hasClass('is-active');

        //close others
        accordion.find('.is-active').removeClass('is-active');

        if (isOpen) {
            item.removeClass('is-active');
        } else {
            item.addClass('is-active');
        }   
    }

    me.init();
};

(function (cjs, an) {
var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];
// symbols:
(lib.Symbol5 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#2BD2CA").s().p("Ag1A1QgVgWAAgfQAAgeAVgWQAXgWAeAAQAfAAAVAWQAXAWAAAeQAAAfgXAWQgVAWgfAAQgeAAgXgWg");
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.5,-7.5,15,15);
(lib.Symbol4 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#F5F9FB").s().p("AoEIEQjVjWAAkuQAAktDVjXQDXjVEtAAQEvAADVDVQDWDXAAEtQAAEujWDWQjVDWkvAAQktAAjXjWg");
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-73,-73,146,146);
(lib.Symbol10 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_1
    this.instance = new lib.Symbol5("synched",0);
    this.instance.parent = this;
    this.instance.setTransform(0,-76.4,1.2,1.2);
    this.timeline.addTween(cjs.Tween.get(this.instance).to({y:0},20,cjs.Ease.get(-1)).wait(1));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-9,-85.4,18,18);
// stage content:
(lib.credify_mid_3 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
    // Layer_3 (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    mask.graphics.p("ArBENQAwi2CNiNQDXjWEtAAQEvAADVDWQCOCNAwC2g");
    mask.setTransform(73,26.9);
    // Symbol 10
    this.instance = new lib.Symbol10("synched",0,false);
    this.instance.parent = this;
    this.instance.setTransform(66.4,66.7);
    this.instance._off = true;
    var maskedShapeInstanceList = [this.instance];
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(64).to({_off:false},0).to({_off:true},21).wait(35));
    // Symbol 10
    this.instance_1 = new lib.Symbol10("synched",0,false);
    this.instance_1.parent = this;
    this.instance_1.setTransform(54.4,66.7);
    this.instance_1._off = true;
    var maskedShapeInstanceList = [this.instance_1];
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(68).to({_off:false},0).to({_off:true},21).wait(31));
    // Symbol 10
    this.instance_2 = new lib.Symbol10("synched",0,false);
    this.instance_2.parent = this;
    this.instance_2.setTransform(90.4,66.7);
    this.instance_2._off = true;
    var maskedShapeInstanceList = [this.instance_2];
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(72).to({_off:false},0).to({_off:true},21).wait(27));
    // Symbol 10
    this.instance_3 = new lib.Symbol10("synched",0,false);
    this.instance_3.parent = this;
    this.instance_3.setTransform(62.4,66.7);
    this.instance_3._off = true;
    var maskedShapeInstanceList = [this.instance_3];
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
    this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(76).to({_off:false},0).to({_off:true},21).wait(23));
    // Symbol 10
    this.instance_4 = new lib.Symbol10("synched",0,false);
    this.instance_4.parent = this;
    this.instance_4.setTransform(86.4,66.7);
    this.instance_4._off = true;
    var maskedShapeInstanceList = [this.instance_4];
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(80).to({_off:false},0).to({_off:true},21).wait(19));
    // Symbol 10
    this.instance_5 = new lib.Symbol10("synched",0,false);
    this.instance_5.parent = this;
    this.instance_5.setTransform(70.4,66.7);
    this.instance_5._off = true;
    var maskedShapeInstanceList = [this.instance_5];
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
    this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(84).to({_off:false},0).to({_off:true},21).wait(15));
    // Symbol 10
    this.instance_6 = new lib.Symbol10("synched",0,false);
    this.instance_6.parent = this;
    this.instance_6.setTransform(66.4,66.7);
    this.instance_6._off = true;
    var maskedShapeInstanceList = [this.instance_6];
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(4).to({_off:false},0).to({_off:true},21).wait(95));
    // Symbol 10
    this.instance_7 = new lib.Symbol10("synched",0,false);
    this.instance_7.parent = this;
    this.instance_7.setTransform(86.4,66.7);
    this.instance_7._off = true;
    var maskedShapeInstanceList = [this.instance_7];
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
    this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(8).to({_off:false},0).to({_off:true},21).wait(91));
    // Symbol 10
    this.instance_8 = new lib.Symbol10("synched",0,false);
    this.instance_8.parent = this;
    this.instance_8.setTransform(78.4,66.7);
    this.instance_8._off = true;
    var maskedShapeInstanceList = [this.instance_8];
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(12).to({_off:false},0).to({_off:true},21).wait(87));
    // Symbol 10
    this.instance_9 = new lib.Symbol10("synched",0,false);
    this.instance_9.parent = this;
    this.instance_9.setTransform(66.4,66.7);
    this.instance_9._off = true;
    var maskedShapeInstanceList = [this.instance_9];
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
    this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(16).to({_off:false},0).to({_off:true},21).wait(83));
    // Symbol 10
    this.instance_10 = new lib.Symbol10("synched",0,false);
    this.instance_10.parent = this;
    this.instance_10.setTransform(82.4,66.7);
    this.instance_10._off = true;
    var maskedShapeInstanceList = [this.instance_10];
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(20).to({_off:false},0).to({_off:true},21).wait(79));
    // Symbol 10
    this.instance_11 = new lib.Symbol10("synched",0,false);
    this.instance_11.parent = this;
    this.instance_11.setTransform(70.4,66.7);
    this.instance_11._off = true;
    var maskedShapeInstanceList = [this.instance_11];
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
        maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
    this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(24).to({_off:false},0).to({_off:true},21).wait(75));
    // Layer_16
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#CEDAE1").s().p("AgfAgQgNgOAAgSQAAgRANgOQANgOASAAQATAAANAOQANAOAAARQAAASgNAOQgNANgTAAQgSAAgNgNg");
    this.shape.setTransform(94,78.4);
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#D5E1E7").s().p("Ak1AQIAAgfIJoAAIADASIAAANg");
    this.shape_1.setTransform(78,55.4);
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#FFFFFF").s().p("AkWE5QhjAAgBhkIAAmpQABhkBjAAIIHAAIAKAAQBIAEAPA/IpoAAIAAAhIJrAAIAABVIiaAAQgwAAgnAlQglAmAAA0QAAA0AlAlQAlAkAyACICaAAIAABWQAABfhaAFgAC+BfQglgBgbgbQgcgcAAgnQAAgmAcgdQAbgaAlgBIC8AAIAAC9gACggeQgOANAAASQAAASAOANQANAOASAAQATAAANgOQANgNAAgSQAAgSgNgNQgNgOgTAAQgSAAgNAOg");
    this.shape_2.setTransform(74.9,78.3);
    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(120));
    // Layer_17
    this.instance_12 = new lib.Symbol4("synched",0);
    this.instance_12.parent = this;
    this.instance_12.setTransform(73,73);
    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(120));
}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(73,73,146,146);
// library properties:
lib.properties = {
    id: '62F3A9FE8F1E4A41B83DEFD6F0448D9B',
    width: 146,
    height: 146,
    fps: 25,
    color: "#FFFFFF",
    opacity: 0.00,
    manifest: [],
    preloads: []
};
// bootstrap callback support:
(lib.Stage = function(canvas) {
    createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();
p.setAutoPlay = function(autoPlay) {
    this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }
p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }
an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
    an.bootstrapListeners=[];
}
an.bootstrapCallback=function(fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if(an.bootcompsLoaded.length > 0) {
        for(var i=0; i<an.bootcompsLoaded.length; ++i) {
            fnCallback(an.bootcompsLoaded[i]);
        }
    }
};
an.compositions = an.compositions || {};
an.compositions['62F3A9FE8F1E4A41B83DEFD6F0448D9B'] = {
    getStage: function() { return exportRoot.getStage(); },
    getLibrary: function() { return lib; },
    getSpriteSheet: function() { return ss; },
    getImages: function() { return img; }
};
an.compositionLoaded = function(id) {
    an.bootcompsLoaded.push(id);
    for(var j=0; j<an.bootstrapListeners.length; j++) {
        an.bootstrapListeners[j](id);
    }
}
an.getComposition = function(id) {
    return an.compositions[id];
}
})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

//---------------------------------------------------------------//
//

var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
function credify_icon3__init() {
    canvas = document.getElementById("credify_icon3__canvas");
    anim_container = document.getElementById("credify_icon3__animation_container");
    dom_overlay_container = document.getElementById("credify_icon3__dom_overlay_container");
    var comp=AdobeAn.getComposition("62F3A9FE8F1E4A41B83DEFD6F0448D9B");
    var lib=comp.getLibrary();
    credify_icon3__handleComplete({},comp);
}
function credify_icon3__handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    exportRoot = new lib.credify_mid_3();
    stage = new lib.Stage(canvas);  
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
        stage.addChild(exportRoot);
        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    }       
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {      
        var lastW, lastH, lastS=1;      
        window.addEventListener('resize', resizeCanvas);        
        resizeCanvas();     
        function resizeCanvas() {           
            var w = lib.properties.width, h = lib.properties.height;            
            var iw = window.innerWidth, ih=window.innerHeight;          
            var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;          
            if(isResp) {                
                if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
                    sRatio = lastS;                
                }               
                else if(!isScale) {                 
                    if(iw<w || ih<h)                        
                        sRatio = Math.min(xRatio, yRatio);              
                }               
                else if(scaleType==1) {                 
                    sRatio = Math.min(xRatio, yRatio);              
                }               
                else if(scaleType==2) {                 
                    sRatio = Math.max(xRatio, yRatio);              
                }           
            }           
            canvas.width = w*pRatio*sRatio;         
            canvas.height = h*pRatio*sRatio;
            canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';               
            canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
            stage.scaleX = pRatio*sRatio;           
            stage.scaleY = pRatio*sRatio;           
            lastW = iw; lastH = ih; lastS = sRatio;            
            stage.tickOnUpdate = false;            
            stage.update();            
            stage.tickOnUpdate = true;      
        }
    }
    makeResponsive(true,'height',true,1); 
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
}
